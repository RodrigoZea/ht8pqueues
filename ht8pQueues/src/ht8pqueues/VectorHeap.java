/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht8pqueues;

import java.util.Vector;

/**
 * Clase que implementa PriorityQueue...
 * @author USER
 */
public class VectorHeap<E extends Comparable<E>> implements PriorityQueue<E>
{

	protected Vector<E> data; // the data, kept in heap order

    /**
     * Construye una nueva PriorityQueue
     */
    public VectorHeap()
	// post: constructs a new priority queue
	{
		data = new Vector<E>();
	}

    /**
     * Construye una nueva PriorityQueue desde un vector desordenado
     * @param v: el vector desordenado
     */
    public VectorHeap(Vector<E> v)
	// post: constructs a new priority queue from an unordered vector
	{
		int i;
		data = new Vector<E>(v.size()); // we know ultimate size
		for (i = 0; i < v.size(); i++)
		{ // add elements to heap
			add(v.get(i));
		}
	}

    /**
     * Retorna el parent del nodo en la ubicacion i
     * @param i: la ubicacion que se busca
     * @return el parent del nodo en esa ubicacion
     */
    protected static int parent(int i)
	// pre: 0 <= i < size
	// post: returns parent of node at location i
	{
		return (i-1)/2;
	}

    /**
     * Retorna el indice del hijo izquierdo del nodo en la ubicacion i
     * @param i: la ubicacacion que se busca
     * @return el indice del hijo izquierdo
     */
    protected static int left(int i)
	// pre: 0 <= i < size
	// post: returns index of left child of node at location i
	{
		return 2*i+1;
	}

    /**
     * Retorna el indice del hijo derecho del nodo en la ubicacion i
     * @param i: la ubicacacion que se busca
     * @return el indice del hijo derecho
     */
    protected static int right(int i)
	// pre: 0 <= i < size
	// post: returns index of right child of node at location i
	{
		return (2*i+1) + 1;
	}

    /**
     * Al comparar el valor cambia el padre dependiendo del valor que este tomo. 
     * @param leaf
     */
    protected void percolateUp(int leaf)
	// pre: 0 <= leaf < size
	// post: moves node at index leaf up to appropriate position
	{
		int parent = parent(leaf);
		E value = data.get(leaf);
		while (leaf > 0 &&
		(value.compareTo(data.get(parent)) < 0))
		{
			data.set(leaf,data.get(parent));
			leaf = parent;
			parent = parent(leaf);
		}
		data.set(leaf,value);
	}

    /**
     * Agrega un valor al vector heap y posteriormente ejecuta percolateUp para mantener orden del Heap
     * @param value valor de elemento generico.
     */
    public void add(E value)
	// pre: value is non-null comparable
	// post: value is added to priority queue
	{
		data.add(value);
		percolateUp(data.size()-1);
	}

    /**
     *
     * @param root
     */
    protected void pushDownRoot(int root)
	// pre: 0 <= root < size
	// post: moves node at index root down
	// to appropriate position in subtree
	{
		int heapSize = data.size();
		E value = data.get(root);
		while (root < heapSize) {
			int childpos = left(root);
			if (childpos < heapSize)
			{
				if ((right(root) < heapSize) &&
				((data.get(childpos+1)).compareTo
				(data.get(childpos)) < 0))
				{
					childpos++;
				}
			// Assert: childpos indexes smaller of two children
			if ((data.get(childpos)).compareTo
				(value) < 0)
			{
				data.set(root,data.get(childpos));
				root = childpos; // keep moving down
			} else { // found right location
				data.set(root,value);
				return;
			}
			} else { // at a leaf! insert and halt
				data.set(root,value);
				return;
			}
		}
	}

    /**
     * elimina el primer paciente.
     * @return
     */
    public E remove()
	// pre: !isEmpty()
	// post: returns and removes minimum value from queue
	{
		E minVal = getFirst();
		data.set(0,data.get(data.size()-1));
		data.setSize(data.size()-1);
		if (data.size() > 1) pushDownRoot(0);
		return minVal;
	}

    /**
     *
     * @return
     */
    @Override
    public E getFirst() {
        return data.get(0);
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        boolean empty = false;
        if (data.size() != 0) { 
            empty = true;
        }
        
        return empty;
    }

    /**
     * retorna el tamaño del vector
     * @return
     */
    @Override
    public int size() {
        return data.size();
    }

    /**
     *.... no necesario de implementar en este programa.
     */
    @Override
    public void clear() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
} 
