/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht8pqueues;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.PriorityQueue;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author USER
 */
public class Ht8pQueues {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        OperacionesHospital ops = new OperacionesHospital();
        VectorHeap<Paciente> vHeap = new VectorHeap<>();
        PriorityQueue<Paciente> jfcImp = new PriorityQueue<>();
         //---Leer archivo--//
        //Lector de texto
        BufferedReader reader;
        //Escoge archivo
        JFileChooser menu;
        //Filtro para archivo unicamente TXT
        FileNameExtensionFilter txtOnly;
        Scanner rdr = new Scanner(System.in);
            
        menu = new JFileChooser();
        txtOnly= new FileNameExtensionFilter("TEXT FILES", "txt", "text");
        menu.setFileFilter(txtOnly);    
        
        int returnV = menu.showOpenDialog(null);
        
        if (returnV == JFileChooser.APPROVE_OPTION){ 
        //---Leer archivo--//        
        System.out.println("Ingrese la implementacion a utilizar:");
        System.out.println("1. Creada por el equipo");
        System.out.println("2. JFC"); 
        String opt = rdr.next();
        
            try { 
                reader = new BufferedReader(new FileReader(menu.getSelectedFile().getAbsoluteFile()));  
            
                String leer = "";
                    
                if (opt.equals("1")){ 
                    while ((leer = reader.readLine()) != null){ 
                        ops.addToHeap(leer, vHeap);
                    }
                ops.printHeap(vHeap);
                    
                }else if(opt.equals("2")){
                    while ((leer = reader.readLine()) != null){ 
                        ops.addToJFCimp(leer, jfcImp);
                    }
                    ops.printJFCimp(jfcImp);       
                }
  
              } catch (IOException ex) {
                System.out.println("ERROR al leer archivo.");
            }                  
        }else{ 
            System.out.println("Operacion cancelada.");
            System.exit(0);
        }       
    }  
}
