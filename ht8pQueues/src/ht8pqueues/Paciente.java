/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht8pqueues;

/**
 *
 * @author Rodrigo Zea y Francisco Molina
 */
public class Paciente implements Comparable<Paciente> {
    String nombre;
    String sintoma;
    String prioridad;
    
    /**
     * Constructor de paciente, se modela lo deseado segun el programa.
     * @param nombre
     * @param sintoma
     * @param prioridad 
     */
    public Paciente(String nombre, String sintoma, String prioridad){ 
        this.nombre = nombre;
        this.sintoma = sintoma;
        this.prioridad = prioridad;
    }
    /**
     * retorna nombre
     * @return 
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * set al nombre del paciente
     * @param nombre 
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * retorna el sintoma del paciente
     * @return 
     */
    public String getSintoma() {
        return sintoma;
    }
    /**
     * set al sintoma del paciente
     * @param sintoma 
     */
    public void setSintoma(String sintoma) {
        this.sintoma = sintoma;
    }
    /**
     * retorna la prioridad definida para el paciente
     * @return 
     */
    public String getPrioridad() {
        return prioridad;
    }
    /**
     * le asigna una prioridad..
     * @param prioridad 
     */
    public void setPrioridad(String prioridad) {
        this.prioridad = prioridad;
    }
    /**
     * imprime el paciente
     * @return 
     */
    public String toString(){ 
        return nombre + "," + sintoma + "," + prioridad;
    }
    /**
     * compara las prioridades de los pacientes.
     * @param pac
     * @return 
     */
    @Override
    public int compareTo(Paciente pac) {
        return this.getPrioridad().compareTo(pac.getPrioridad());
    }
    
}
