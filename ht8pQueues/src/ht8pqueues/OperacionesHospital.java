/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht8pqueues;

import java.util.PriorityQueue;

/**
 *
 * @author Rodrigo Zea y Francisco Molina
 */
public class OperacionesHospital {
    
    /**
     * Agrega un dato al vectorHeap de Pacientes en el caso de este programa.
     * @param leer: Linea de texto del archivo que esta leyendo
     * @param vHeap: El VectorHeap o cola de prioridad de tipo paciente
     */
    public int addToHeap(String leer, VectorHeap<Paciente> vHeap){ 
        int val = 1;
        String[] pacienteInfo = leer.split(",");
                    
        String name = pacienteInfo[0];
        String sintoma = pacienteInfo[1];
        String prioridad = pacienteInfo[2];
                    
        Paciente nPaciente = new Paciente(name, sintoma, prioridad);        
        vHeap.add(nPaciente);
        return val;
   }
    
    /**
     * Imprime todos los datos del VectorHeap o cola de prioridad, en orden
     * @param vHeap: el VectorHeap o cola de prioridad de tipo paciente
     * @return 
     */
    public String printHeap(VectorHeap<Paciente> vHeap){ 
       int cantInicial = vHeap.size();
       String msj ="";         
       for (int i = 0; i < cantInicial; i++) {
            String msjTemp=vHeap.getFirst().toString();
            System.out.println(vHeap.getFirst().toString());
            vHeap.remove();
            msj=msj+msjTemp;
       }
       return msj;
   }
    public void delete(VectorHeap<Paciente> vHeap){
        vHeap.remove();
    }
   
    /**
     * Agrega un dato a la implementacion de cola de prioridad del JFC
     * @param leer: Linea de texto del archivo que esta leyendo
     * @param jfcImp: La cola de prioridad, implementacion proveida por JFC
     */
    public void addToJFCimp(String leer, PriorityQueue<Paciente> jfcImp){
       String[] pacienteInfo = leer.split(",");
                    
       String name = pacienteInfo[0];
       String sintoma = pacienteInfo[1];
       String prioridad = pacienteInfo[2];
                    
       Paciente nPaciente = new Paciente(name, sintoma, prioridad);
                    
       jfcImp.add(nPaciente);
   }
   
    /**
     * Imprime todos los datos de la cola de prioridad proveida por JFC, en orden
     * @param jfcImp: La cola de prioridad, implementacion proveida por JFC
     */
    public void printJFCimp(PriorityQueue<Paciente> jfcImp){ 
       int cantInicial = jfcImp.size();
                    
       for (int i = 0; i < cantInicial; i++) {
            System.out.println(jfcImp.poll());
       }
   }
}
