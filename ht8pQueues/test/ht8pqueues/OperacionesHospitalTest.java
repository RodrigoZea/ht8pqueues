/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ht8pqueues;

import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Francisco Molina
 */
public class OperacionesHospitalTest {
    
    public OperacionesHospitalTest() {
    
    }
   
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     * Test of addToHeap method, of class OperacionesHospital.
     */
    @Test
    public void testaddToHeap() {
        System.out.println("addToHeap");
        String leer="Francisco Molina, dolor de cabello , alta";
        int expResult = 1;
        OperacionesHospital instance = new OperacionesHospital();
        VectorHeap vectorTest = new VectorHeap();
        instance.addToHeap(leer, vectorTest);
        int result=vectorTest.size();
        System.out.println(result);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.    
    }
    
    /**
     * Test of testprintHeap method, of class OperacionesHospital.
     */
    @Test
    public void testprintHeap() {
        System.out.println("printHeap");
        String leer="Francisco Molina,dolor de cabello, alta";
        String expResult = leer;
        OperacionesHospital instance = new OperacionesHospital();
        VectorHeap vectorTest = new VectorHeap();
        instance.addToHeap(leer, vectorTest);
        String result=instance.printHeap(vectorTest);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }
    
     @Test
    public void delete() {
        System.out.println("delete");
        OperacionesHospital instance = new OperacionesHospital();
        VectorHeap vectorTest = new VectorHeap();
        int result=vectorTest.size();
        assertEquals(0, result);
        // TODO review the generated test code and remove the default call to fail.
        
    }

    
}
